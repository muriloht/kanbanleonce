angular.module('starter')

.factory("testService", ['$soap',function($soap){
   var base_url = "http://localhost:8080/ADInterface/services/ModelADService";
  
   
    return {
        GetUsers: function(param){
            return $soap.post(base_url,"",param 
            		);
        }
    };
}])

.controller('TabsCtrl', function ($scope, $rootScope){
})

.controller('soapLocationCtrl', function($scope,$rootScope, testService){
	$scope.call = function (){ 
		  var param = {"_0:queryData":
		   { "_0:ModelCRUDRequest":{
		       "_0:ModelCRUD":{
		           "_0:serviceType": "GetProducts", 
		           "_0:TableName":"M_Product" 
		                   }, 
		       "_0:ADLoginRequest": {
		           "_0:user":"WebService",
		           "_0:pass":"WebService",
		           "_0:lang":"192",
		           "_0:ClientID":"11",
		           "_0:RoleID":"50004",
		           "_0:OrgID":"11",
		           "_0:WarehouseID":"103",
		           "_0:stage":"0"}
		}}
		           };
		  testService.GetUsers(param).then(function(response){
			  console.log(JSON.stringify(response));
			  console.log(response);
			  var x2js = new X2JS();
			  var jsonObject = x2js.xml2json(response);
			  console.log(JSON.stringify(jsonObject));
			  
	            $scope.vic= [];
				
				var  array= jsonObject.Envelope.Body.queryDataResponse.WindowTabData.DataSet.DataRow;    
			    for (var i = 0; i<array.length; i++){
			  console.log(array[i]); 
			  var message = {};
			
			  message.name = array[i].field.val;
			  $scope.vic.push(message);
			  // alert(JSON.stringify(message));
		  }
		  }
		  );
	  };
	
	
})
.controller("soapCreateCtrl", function($scope,$rootScope, testService) {
//	var parser = require('xml2json');
	
  $scope.createpara = {"value": '',
		               "name": '',
		               "taxid": '',
		               "isvendor": '', 
		               "iscustomer": '',
		               "istaxexempt": '',
		               "name2": '',
		               "groupid": ''
		            	   
	                   };
	
	$scope.create = function (){ 
	
	var createparam= {"_0:createData":
		   { "_0:ModelCRUDRequest":{
		       "_0:ModelCRUD":{
		           "_0:serviceType": "CreateBPartner", 
		           
		           "_0:DataRow":{
		        	   "_0:field column =\"Value\"" :
		        	   {"_0:val":$scope.createpara.value},
		        	   "_0:field column =\"Name\"" :
		        	   {"_0:val":$scope.createpara.name},
		        	   "_0:field column =\"TaxID\"" :
		        	   {"_0:val":$scope.createpara.taxid},
		        	   "_0:field column =\"IsVendor\"" :
		        	   {"_0:val":$scope.createpara.isvendor},
		        	   "_0:field column =\"IsCustomer\"" :
		        	   {"_0:val":$scope.createpara.iscustomer},
		        	   "_0:field column =\"IsTaxExempt\"" :
		        	   {"_0:val":$scope.createpara.istaxexempt},
		        	   "_0:field column =\"Name2\"" :
		        	   {"_0:val":$scope.createpara.name2},
		        	   "_0:field column =\"C_BP_Group_ID\"" :
		        	   {"_0:val":$scope.createpara.groupid}
		        	   
		           }          
		                   }, 
		                   
		                   
		       "_0:ADLoginRequest": {
		           "_0:user":"WebService",
		           "_0:pass":"WebService",
		           "_0:lang":"192",
		           "_0:ClientID":"11",
		           "_0:RoleID":"50004",
		           "_0:OrgID":"11",
		           "_0:WarehouseID":"103",
		           "_0:stage":"0"}
		}}
		           };
		
	  testService.GetUsers(createparam).then(function(response){
		  console.log(JSON.stringify(response));
		  console.log(response);
		  var x2js = new X2JS();
		  var jsonObject = x2js.xml2json(response);
		  console.log(JSON.stringify(jsonObject));
		  
           // $scope.vic= jsonObject.Envelope.Body.queryDataResponse.WindowTabData.DataSet.DataRow;  
		  
	  }
	  );
  };

})

.controller('soapGetCtrl', function($scope,$rootScope,$ionicLoading, KLDataFactory) {
    
	
//	var parser = require('xml2json');
  $scope.call = function (){ 
	  var param;
	  $ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	$scope.bool = false;
	$scope.showme = function (){
		   $scope.bool =  !$scope.bool; 
		   
	}
	   param = {"_0:queryData":
	   { "_0:ModelCRUDRequest":{
	       "_0:ModelCRUD":{
	           "_0:serviceType": "getBPartners", 
	           "_0:TableName":"C_BPartner" 
	                   }, 
	       "_0:ADLoginRequest": {
	           "_0:user":"WebService",
	           "_0:pass":"WebService",
	           "_0:lang":"192",
	           "_0:ClientID":"11",
	           "_0:RoleID":"50004",
	           "_0:OrgID":"11",
	           "_0:WarehouseID":"103",
	           "_0:stage":"0"}
	}}
	           };
			   
	  KLDataFactory.GetUsers(param).then(function(response){
		  console.log(JSON.stringify(response));
		  console.log(response);
		   $ionicLoading.hide();
		  var x2js = new X2JS();
		  var jsonObject = x2js.xml2json(response);
		  console.log(JSON.stringify(jsonObject));
		  
           var  array= jsonObject.Envelope.Body.queryDataResponse.WindowTabData.DataSet.DataRow;  
		   $scope.vic  = [];
           for (var i = 0; i<array.length; i++){
			  console.log(array[i]); 
			  var message = {};
			  message.id = array[i].field[0].val;
			  message.name = array[i].field[1].val;
			  $scope.vic.push(message);
			  // alert(JSON.stringify(message));
		  }
	  }, function(error) {
		        console.log(error)
                 $ionicLoading.hide();
				 alert(error);
                }
	  );
  };
})
.controller('soapUdateCtrl', function($scope, testService){
	$scope.updateparam = {};
	$scope.updateparam.recordid = "";
	$scope.updateparam.name = ""
	$scope.update = function (){
		
		var createparam= {"_0:updateData":
		   { "_0:ModelCRUDRequest":{
		       "_0:ModelCRUD":{
		           "_0:serviceType": "UpdateBPartner", 
		           "_0:RecordID": $scope.updateparam.recordid,
		           "_0:DataRow":{
		        	   "_0:field column =\"Name\"" :
		        	   {"_0:val":$scope.updateparam.name}
		        	   
		           }          
		                   }, 
		                   
		                   
		       "_0:ADLoginRequest": {
		           "_0:user":"WebService",
		           "_0:pass":"WebService",
		           "_0:lang":"192",
		           "_0:ClientID":"11",
		           "_0:RoleID":"50004",
		           "_0:OrgID":"11",
		           "_0:WarehouseID":"103",
		           "_0:stage":"0"}
		}}
		           };
		
	  testService.GetUsers(createparam).then(function(response){
		  console.log(JSON.stringify(response));
		  console.log(response);
		  var x2js = new X2JS();
		  var jsonObject = x2js.xml2json(response);
		  console.log(JSON.stringify(jsonObject));
		  
        // $scope.vic= jsonObject.Envelope.Body.queryDataResponse.WindowTabData.DataSet.DataRow;  
		  
	  }
	  );
};
			
	
           
	
})
.controller('soapCreateUpdateCtrl', function($scope){
	
})

.controller('soapDeleteCtrl', function($scope, testService){
	
	$scope.deleteparam= {};
	$scope.deleteparam.val = "" ;
	$scope.deleteBp = function (){
	
	
		
		var param = {
				"_0:deleteData":
		
		{ "_0:ModelCRUDRequest":{
			"_0:ModelCRUD":{
				"_0:serviceType": "DeleteBPartners", 
				"_0:RecordID":$scope.deleteparam.val
			}, 
			"_0:ADLoginRequest": {
				"_0:user":"WebService",
				"_0:pass":"WebService",
				"_0:lang":"192",
				"_0:ClientID":"11",
				"_0:RoleID":"50004",
				"_0:OrgID":"11",
				"_0:WarehouseID":"103",
				"_0:stage":"0"}
		}}
		};
		
		
		
		
		
		
		testService.GetUsers(param).then(function(response){
			  console.log(JSON.stringify(response));
			  console.log(response);
			  var x2js = new X2JS();
			  var jsonObject = x2js.xml2json(response);
			  console.log(JSON.stringify(jsonObject));
		});
	
	}
	
	
	
		
	
})