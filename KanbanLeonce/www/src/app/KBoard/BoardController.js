
angular.module('starter')
.controller("SlideBoxController", function($scope, $ionicSlideBoxDelegate){
  
	
	$scope.navSlide= function() {
    $ionicSlideBoxDelegate.next(); // slide to whatever index clicked on and perform 500 milli second transition
  };
})



//creates todo list elements
.controller('TodoListCtrl', function($scope,$rootScope,$timeout,$ionicSlideBoxDelegate,$ionicPopup,$ionicLoading, $ionicModal, KLDataFactory) {
	
	var lol = [];
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	$timeout(function(){
		 $ionicLoading.hide();
	}, 5000)
	
	$scope.populate = function (){
		$rootScope.CreditStop =[];
		$rootScope.CreditOK = [];
		$rootScope.CreditWatch = [];
		 $rootScope.CreditHold= [];
		 $rootScope.NoCreditCheck = [];
	KLDataFactory.GetCreditStatus().then(function(response){
		
		var x2js = new X2JS();
		  var jsonObject = x2js.xml2json(response);
		  var test =  jsonObject;
		  lol = test.Envelope.Body.queryDataResponse.WindowTabData.DataSet.DataRow;
		  console.log(lol[0].field[0]);
		  for (i = 0 ; i < lol.length; i++){
			     
			  var mess = {
					  title: lol[i].field[1].val
			  }
			  var key = lol[i].field[1].val
			  switch (key) {
			  case 'S':
				$rootScope.CreditStop.push(mess);
				
				break;
			  
			  case 'O':
				$rootScope.CreditOK.push(mess);
               break;
			  
			  case 'W':
				  $rootScope.CreditWatch.push(mess);
               break;

			  case 'H':
				  $rootScope.CreditHold.push(mess);
               break;
			  
			  case 'X':
				$rootScope.NoCreditCheck.push(mess);
               break;
                 
			default:
			$rootScope.CreditStop.push(mess);
				break;
			}
              $scope.$broadcast('scroll.refreshComplete');
//			  $rootScope.CreditStop.push(lol[i])
			  $ionicLoading.hide();
		  }
		  }).catch(function(){
		$ionicLoading.hide()
		var alertPopup = $ionicPopup
		.alert({
			title : 'ERROR!!!!1'
				+ user,
				template : '101'
					+ user
		});
	});
	
	}
  // No need for testing data anymore
  $rootScope.CreditStop = [];
  $rootScope.status=[];
  
  // Create and load the Modal
  $ionicModal.fromTemplateUrl('src/app/KBoard/new-step.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Called when the form is submitted
  $scope.createTask = function(task) {
    $rootScope.CreditStop.push({
      title: task.title
    });
    $scope.taskModal.hide();
    task.title = "";
  };

  // Open our new task modal
  $scope.newTask = function() {
    $scope.taskModal.show();
  };


  // Close the new task modal
  $scope.closeNewTask = function() {
    $scope.taskModal.hide();
  };
})


//inprogress controller

.controller("inProgressCtrl", function($scope,$rootScope){
 $rootScope.CreditOK=[];
	
})


.controller("CreditWatchCtrl", function($scope,$rootScope){
 $rootScope.CreditWatch=[];
	
})


.controller("NoCreditCheckCtrl", function($scope,$rootScope){
 $rootScope.NoCreditCheck=[];
	
})


.controller("CreditHoldCtrl", function($scope,$rootScope){
 $rootScope.CreditHold=[];
	
})


//drag drop property controller
.controller('MainCtrl', function ($scope, $rootScope,$timeout, $ionicScrollDelegate, $ionicSlideBoxDelegate) {
    
	
	function getslideindex() {
		
		return $ionicSlideBoxDelegate.currentIndex();
	};
	var prevslideindex;	    
	  
    function startup(){
	    $scope.timeSinceLastScroll = new Date().getTime();
	  }
	  
	  startup();
	  
	  $scope.cardRemoved = function (data, event, option){
	    
	    console.log("drag success data" + option + JSON.stringify(data));
	
		  
	    
	    switch (prevslideindex) {
		  case 0:
			  var index = $rootScope.CreditStop.indexOf(data);
			  if (index > -1){
				  $rootScope.CreditStop.splice(index, 1);
				  
			  }
			  console.log("Tasks" + index);
			  break;
		  case 1:
			  var index = $rootScope.CreditOK.indexOf(data);
			  if (index > -1){
				  $rootScope.CreditOK.splice(index, 1);
				  
			  }
			  console.log("Tasks" + index);
			  
			  break;
		  case 2:
			  var index = $rootScope.CreditWatch.indexOf(data);
			  if (index > -1) {
				  
				  $rootScope.CreditWatch.splice(index,1);
			  }
			  
			  console.log("CW" + index);
			  break;
		  case 3:
			  var index = $rootScope.NoCreditCheck.indexOf(data);
			  if (index > -1) {
				  $rootScope.NoCreditCheck.splice(index,1);
				  
			  }
			  
			  console.log("NCC" + index);
			  break;
			  
		  case 4:
			  var index = $rootScope.CreditHold.indexOf(data);
			  if (index > -1){
				  $rootScope.CreditHold.splice(index,1);
				  
			  }
			  
			  console.log("CH" + index);
			  break;

		default:
			break;
		}
	            
	            
	  };
	  
	  $scope.addCard = function(data, event){
		 
		  
		  function getslideindex() {
				
				return $ionicSlideBoxDelegate.currentIndex();
			};
			  
			var slideindex  = getslideindex();
			console.log(slideindex);
		  
		  switch (slideindex) {
		  case 0:
			  var index = $rootScope.CreditStop.indexOf(data);
			  if (index == -1){
				  $rootScope.CreditStop.unshift(data);
				  
			  }
			  console.log("Tasks" + index);
			  break;
		  case 1:
			  var index = $rootScope.CreditOK.indexOf(data);
			  if (index == -1){
				  $rootScope.CreditOK.unshift(data);
				  
			  }
			  console.log("Progress" + index);
			  
			  break;
		  case 2:
			  var index = $rootScope.CreditWatch.indexOf(data);
			  if (index == -1){
				  $rootScope.CreditWatch.unshift(data);
				  
			  }
			  
			  console.log("CW" + index);
			  break;
		  case 3:
			  var index = $rootScope.NoCreditCheck.indexOf(data);
			  if (index == -1){
				  $rootScope.NoCreditCheck.unshift(data);
				  
			  }
			  
			  console.log("NCC" + index);
			  break;
		  case 4:
			  var index = $rootScope.CreditHold.indexOf(data);
			  if (index == -1) {
				  $rootScope.CreditHold.unshift(data);
				  
			  }
			  
			  console.log("CH" + index);
			  break;

		default:
			break;
		}
	  };
	  
	  
	     
	    $scope.stopSlide = function(){
	      $ionicSlideBoxDelegate.enableSlide(false);
	      prevslideindex  = getslideindex();
	    };
	    
	    $scope.startSlide = function(){
	      $ionicSlideBoxDelegate.enableSlide(true);
	    };
	   
	   $scope.dragAction = function(attributeCase){
	     
	     var triggerTime = new Date().getTime();
	     
	     if(triggerTime - $scope.timeSinceLastScroll > 900){
	        switch(attributeCase){
	       case 'nextboardleft':
	           $scope.timeSinceLastScroll =   new Date().getTime();
	         $scope.handleSwipeRightGesture();
	         break;
	       case 'nextboardright':
	             $scope.timeSinceLastScroll =   new Date().getTime();
	         $scope.handleSwipeLeftGesture();
	         break;
	      }  
	    }
	     
	    
	   };
	   
	    
	    $scope.handleSwipeRightGesture = function(){
	    	
	    	$timeout(function() {
	    		$ionicScrollDelegate.$getByHandle('listScrollDelegate').scrollBy(-320,0,true);
			  }, 1000);
	    	$ionicSlideBoxDelegate.previous();
	    };
	    
	    $scope.handleSwipeLeftGesture = function(){
	    	$timeout(function() {
	      $ionicScrollDelegate.$getByHandle('listScrollDelegate').scrollBy(320,0,true);
	    },1000);
	    	$ionicSlideBoxDelegate.next();
	    };
	  
	})